package vn.edu.vnuk.jdbc;

import java.sql.SQLException;

import java.sql.Connection;
import java.sql.PreparedStatement;


public class JdbcInsertMotorbikes {

	public static void main(String[] args) throws SQLException {
		Connection connection = new ConnectionFactory().getConnection();
		String sqlQuery = "insert into motorbikes (brand, model, engine_size, gearbox) "
						+ "values (?,?,?,?) ";
		
		
		PreparedStatement statement;
		
		try {
			statement = connection.prepareStatement(sqlQuery);
			statement.setString(1, "SYM");
			statement.setString(2, "Angle" );
			statement.setInt(3, 50);
			statement.setString(4,"Semi-Auto");
			
			statement.execute();
			statement.close();
			System.out.println("New record in DB");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		finally {
			connection.close();
			System.out.println("Done!");
		}
		

	}

}
