package vn.edu.vnuk.jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.List;

import vn.edu.vnuk.jdbc.ConnectionFactory;

import vn.edu.vnuk.jdbc.model.Motorbike;

public class MotorbikeDao {
	
		
		private Connection connection;
		
		public MotorbikeDao() {
			this.connection = new ConnectionFactory().getConnection();
		}
		
		public void create(Motorbike motorbike) throws SQLException {
			String sqlQuery = "insert into motorbikes (brand,model,engine_size,gearbox) "
					+ "values (?,?,?,?) ";


			PreparedStatement statement;
			
			try {
				statement = connection.prepareStatement(sqlQuery);
				statement.setString(1, motorbike.getBrand());
				statement.setString(2, motorbike.getModel() );
				statement.setInt(3, motorbike.getEngineSize());
				
				statement.setString(4,motorbike.getGearBox());
				statement.execute();
				statement.close();
				System.out.println("New record in DB");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			finally {
				connection.close();
				System.out.println("Done!");
			}
				}

		//read(list of contacts)
		@SuppressWarnings("finally")
		public List<Motorbike> read() throws SQLException {
			
			String sqlQuery = "select * from motorbikes";
			
			List<Motorbike> motorbikes = new ArrayList<Motorbike>();

			PreparedStatement statement;
			
			try {
				statement = connection.prepareStatement(sqlQuery);
				
				ResultSet results = statement.executeQuery();
					while(results.next()) {
						Motorbike motorbike = new Motorbike();
						motorbike.setId(results.getLong("id"));
						motorbike.setBrand(results.getString("brand"));
						motorbike.setModel(results.getString("model"));
						motorbike.setEngineSize(results.getInt("engine_size"));
						motorbike.setGearBox(results.getString("gearbox"));
						motorbikes.add(motorbike);
						
					}
				results.close();
				statement.close();
				System.out.println("New record in DB");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			finally {
				connection.close();
				return motorbikes;
			}
		}
			
			@SuppressWarnings("finally")
			public Motorbike read(long id) throws SQLException {
				
				String sqlQuery = "select * from motorbikes where id ='" + id + "'";
				
				Motorbike motorbike = new Motorbike();
				
				PreparedStatement statement;
				
				try {
					statement = connection.prepareStatement(sqlQuery);
					
					ResultSet results = statement.executeQuery();
						while(results.next()) {
							
							motorbike.setId(results.getLong("id"));
							motorbike.setBrand(results.getString("brand"));
							motorbike.setModel(results.getString("model"));
							motorbike.setEngineSize(results.getInt("engine_size"));
							motorbike.setGearBox(results.getString("gearbox"));
							
						}
					results.close();
					statement.close();
					System.out.println("New record in DB");
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				finally {
					connection.close();
					return motorbike;
				}
				
			
			}
			
			public void update() throws SQLException {
				String sqlQuery = "UPDATE motorbikes SET brand = '41blo' WHERE id = '3'" ;
				PreparedStatement statement;
				
				try {
					statement = connection.prepareStatement(sqlQuery);
					
					statement.execute();
					statement.close();
					System.out.println("Successfully updated!");
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					connection.close();
				}
			}
			
			public void destroy() throws SQLException {
				String sqlQuery = "DELETE FROM motorbikes WHERE id = '1'" ;
				PreparedStatement statement;
				
				try {
					statement = connection.prepareStatement(sqlQuery);
					
					statement.execute();
					statement.close();
					System.out.println("Successfully destroyed!");
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					connection.close();
				}
			}
	

}
