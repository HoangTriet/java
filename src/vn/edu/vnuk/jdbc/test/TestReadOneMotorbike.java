package vn.edu.vnuk.jdbc.test;

import java.sql.SQLException;

import vn.edu.vnuk.jdbc.dao.MotorbikeDao;
import vn.edu.vnuk.jdbc.model.Motorbike;

public class TestReadOneMotorbike {
	public static void main(String[] args)  throws SQLException {
	
		MotorbikeDao motorbikeDao = new MotorbikeDao();
		Motorbike motorbike = motorbikeDao.read(1);
		
		System.out.println("ID: " + motorbike.getId());
		System.out.println("Brand: " + motorbike.getBrand());
		System.out.println("Model: " + motorbike.getModel());
		System.out.println("EngineSize: " + motorbike.getEngineSize());
		System.out.println("GearBox: " + motorbike.getGearBox());
	}
}
