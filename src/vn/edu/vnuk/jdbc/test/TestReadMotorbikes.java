package vn.edu.vnuk.jdbc.test;

import java.sql.SQLException;
import java.util.List;

import vn.edu.vnuk.jdbc.dao.MotorbikeDao;
import vn.edu.vnuk.jdbc.model.Motorbike;

public class TestReadMotorbikes {

	public static void main(String[] args)  throws SQLException {
		// TODO Auto-generated method stub
		
		MotorbikeDao motorbikeDao = new MotorbikeDao();
		List<Motorbike> lists = motorbikeDao.read();
		
		for(Motorbike motorbike: lists) {
			System.out.println("ID: " + motorbike.getId());
			System.out.println("Brand: " + motorbike.getBrand());
			System.out.println("Model: " + motorbike.getModel());
			System.out.println("EngineSize: " + motorbike.getEngineSize());
			System.out.println("GearBox: " + motorbike.getGearBox());
		}
	}

}
