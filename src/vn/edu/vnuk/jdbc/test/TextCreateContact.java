package vn.edu.vnuk.jdbc.test;

import java.sql.SQLException;
import java.util.Calendar;

import vn.edu.vnuk.jdbc.dao.ContactDao;
import vn.edu.vnuk.jdbc.model.Contact;

public class TextCreateContact {

	public static void main(String[] args) throws SQLException {
		
		Contact contact = new Contact();
		contact.setName("blo");
		contact.setEmail("blo@gmail.com");
		contact.setAddress("41blo");
		contact.setDateOfBirth(Calendar.getInstance());
	
		
		ContactDao contactDao = new ContactDao();
		contactDao.create(contact);
		
		
	}

}
