package vn.edu.vnuk.jdbc.test;

import java.sql.SQLException;

import vn.edu.vnuk.jdbc.dao.MotorbikeDao;
import vn.edu.vnuk.jdbc.model.Motorbike;

public class TestCreateMotorbikes {
public static void main(String[] args) throws SQLException {
		
		Motorbike motorbike = new Motorbike();
		
		motorbike.setBrand("suzuki");
		motorbike.setModel("jupiter");
		motorbike.setEngineSize(150);
		motorbike.setGearBox("auto");
		
		MotorbikeDao motorbikeDao = new MotorbikeDao();
		motorbikeDao.create(motorbike);
		
	}


}
